# Code for the Creative, Serious and Playful Science of Android Apps Class at Coursera #

### This repository includes two custom applications created for the class: ###

* A Tip Calculator (AndroidApps101-Week3Assignment1-TipCalculator folder) 
* An English-Metric Converter (AndroidApps101Assgnment2App2-Converter folder)

### The course content can be found [here](https://www.coursera.org/course/androidapps101) ###