package system.gc.tipcalculator;

import java.math.BigDecimal;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnSeekBarChangeListener, TextWatcher {
	
	private EditText amount;
	private EditText tip;
	private EditText total;
	private TextView percent;
	private SeekBar percentSeekBar;
	private TipCalculator tipCalculator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		percentSeekBar = (SeekBar)findViewById(R.id.percent_spinner);
		percentSeekBar.setOnSeekBarChangeListener(this);
		
		amount = (EditText)findViewById(R.id.amount_editText);
		amount.addTextChangedListener(this);
		
		tip = (EditText)findViewById(R.id.tip_amount_editText);
		total = (EditText)findViewById(R.id.total_amount_editText);
		percent = (TextView)findViewById(R.id.percent_textView);
		int defaultPercent = percentSeekBar.getProgress();
		percent.setText(Integer.toString(defaultPercent));
		
		tipCalculator = new TipCalculator();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	protected void displayTipAndTotal(String percentage) {
		if (percentage != null && !percentage.equals("")) {
			Double percent = Double.parseDouble(percentage);
			String strAmt = amount.getText().toString();
			if (strAmt != null && !strAmt.equals("") ) {
				try {
					double amt = Double.parseDouble(strAmt);
					BigDecimal tipAmount = tipCalculator.calculateTip(amt, percent);
					tip.setText(tipCalculator.formatCurrency(tipAmount));
					total.setText(tipCalculator.formatCurrency(tipAmount.add(new BigDecimal(strAmt))));
				} catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), getString(R.string.amount_error), Toast.LENGTH_LONG).show();					;
				}
			}
		}
	}
	
	/*********************Start OnSeekBarChangeListener methods ******************************/	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		String strPercent = Integer.toString(progress); 
		displayTipAndTotal(strPercent);
		percent.setText(strPercent);
		//dismiss keyboard if open
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm.isAcceptingText()) {
			imm.hideSoftInputFromWindow(amount.getWindowToken(), 0);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// Not used
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// Not used
	}
	/*********************End OnSeekBarChangeListener methods ******************************/	

	/*********************Start TextWatcher methods ******************************/	
	@Override
	public void afterTextChanged(Editable s) {
		int value = percentSeekBar.getProgress();
		//recalculate tip
		displayTipAndTotal(Integer.toString(value));
		//format value as a currency
//		String strAmount = amount.getText().toString();
//		String formattedAmount = getDollarFormattedNumber(new BigDecimal(Double.parseDouble(strAmount)));
//		amount.setText(formattedAmount);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// Not used
	}
	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// Not used
	}
	/*********************End TextWatcher methods ******************************/	

}
