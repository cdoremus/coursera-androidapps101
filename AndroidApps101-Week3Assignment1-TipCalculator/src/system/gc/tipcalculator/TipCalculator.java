package system.gc.tipcalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

public class TipCalculator {

	public static final int SCALE= 2;
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

	public BigDecimal calculateTip(double amount, double percent ) {
		BigDecimal bdAmount = new BigDecimal(Double.toString(amount));
		bdAmount.setScale(SCALE, ROUNDING_MODE);
		BigDecimal bdPercent = new BigDecimal(Double.toString(percent));
		bdPercent.setScale(SCALE, ROUNDING_MODE);
		
		BigDecimal tipAmount =  bdPercent.divide(new BigDecimal("100.00")).multiply(bdAmount) ; //(percent/100.0)*amount;
		tipAmount.setScale(SCALE, ROUNDING_MODE);
		return tipAmount;
	}
	
	public String formatCurrency(BigDecimal number) {
	    String showsTwoDecimalPlaces = NumberFormat.getCurrencyInstance().format(number);
	    String zeroes = ".00";
	    String lastCharacters = showsTwoDecimalPlaces.substring(Math.max(showsTwoDecimalPlaces.length() - zeroes
	            .length(), 0));
	    return (lastCharacters.equals(zeroes)) ? showsTwoDecimalPlaces.substring(0, 
	            showsTwoDecimalPlaces.length() - zeroes.length()) : showsTwoDecimalPlaces;
	}

}
