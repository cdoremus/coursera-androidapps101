package system.gc.tipcalculator.test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import system.gc.tipcalculator.TipCalculator;
import junit.framework.TestCase;

public class TipCalculatorTest extends TestCase {

	private TipCalculator tipCalculator;
	
	protected void setUp() throws Exception {
		super.setUp();
		tipCalculator = new TipCalculator();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		tipCalculator = null;
	}

	
	public void testTipCalculatorConstructor_Instantiate_ReturnNonNullObject() {
		assertNotNull(tipCalculator);
	}
	
	public void testCalculateTip_Amount100Percent15_TipEquals15() {
		double amount = 100.00;
		double percent = 15.0;
		
		BigDecimal tip = tipCalculator.calculateTip(amount, percent);
		
		BigDecimal expected = new BigDecimal(15.00);
		expected.setScale(TipCalculator.SCALE);
		assertEquals(15.00, tip.doubleValue());
	}


	public void testCalculateTip_Amount10Percent15_TipEquals1pt50() {
		double amount = 10.00;
		double percent = 15.0;
		
		BigDecimal tip = tipCalculator.calculateTip(amount, percent);
		
		BigDecimal expected = new BigDecimal(1.50);
		expected.setScale(TipCalculator.SCALE);
		assertEquals(1.50, tip.doubleValue());
	}

	public void testCalculateTip_AmountZeroPercent15_TipEqualsZero() {
		double amount = 0.00;
		double percent = 15.0;
		
		BigDecimal tip = tipCalculator.calculateTip(amount, percent);
		
		BigDecimal expected = new BigDecimal(1.50);
		expected.setScale(TipCalculator.SCALE);
		assertEquals(0.00, tip.doubleValue());
	}

	public void testFormatCurrentcy_Amount50pt25Percent15_TipEquals7pt54() {
		double amount = 50.25;
		double percent = 15.0;
		
		BigDecimal tip = tipCalculator.calculateTip(amount, percent);
		
		BigDecimal expected = new BigDecimal(7.54);
		expected.setScale(TipCalculator.SCALE, RoundingMode.HALF_UP);
		assertEquals(tipCalculator.formatCurrency(expected), tipCalculator.formatCurrency(tip));
	}
	

}
