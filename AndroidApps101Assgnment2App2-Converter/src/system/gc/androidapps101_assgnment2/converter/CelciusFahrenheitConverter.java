package system.gc.androidapps101_assgnment2.converter;

public class CelciusFahrenheitConverter extends Converter {

	@Override
	public double convertEnglishToMetric(double valueToConvert) {
		return (5d/9d) * (valueToConvert - 32d);
	}

	@Override
	public double convertMetricToEnglish(double valueToConvert) {
		return (9d/5d) * valueToConvert + 32d;
	}

}
