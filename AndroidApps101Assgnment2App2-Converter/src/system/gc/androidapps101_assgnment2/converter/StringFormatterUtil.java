package system.gc.androidapps101_assgnment2.converter;

public class StringFormatterUtil {

	private StringFormatterUtil() {
		// TODO Auto-generated constructor stub
	}

	
	public static String formatDouble(double valueToFormat) {
		return String.format("%1$,.2f", valueToFormat);
	}
}
