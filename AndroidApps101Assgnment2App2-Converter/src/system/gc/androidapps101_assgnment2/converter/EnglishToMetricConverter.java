package system.gc.androidapps101_assgnment2.converter;

public interface EnglishToMetricConverter {

	double convertEnglishToMetric(double valueToConvert);
}
