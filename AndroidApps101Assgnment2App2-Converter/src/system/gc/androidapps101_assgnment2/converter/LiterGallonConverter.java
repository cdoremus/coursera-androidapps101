package system.gc.androidapps101_assgnment2.converter;

public class LiterGallonConverter extends Converter{

	public LiterGallonConverter() {
	}

	@Override
	public double convertMetricToEnglish(double valueToConvert) {
		double litersPerGallon = 1d/3.78541;
		return valueToConvert * litersPerGallon;
	}

	@Override
	public double convertEnglishToMetric(double valueToConvert) {
		double gallonsPerLiter = 3.78541;
		return valueToConvert * gallonsPerLiter;
	}

}
