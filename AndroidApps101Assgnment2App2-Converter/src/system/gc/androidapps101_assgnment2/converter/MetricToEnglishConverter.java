package system.gc.androidapps101_assgnment2.converter;

public interface MetricToEnglishConverter {

	double convertMetricToEnglish(double valueToConvert);
	
}
