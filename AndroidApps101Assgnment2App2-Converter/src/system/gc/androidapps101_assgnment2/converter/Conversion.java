package system.gc.androidapps101_assgnment2.converter;

public enum Conversion {

	KM_MILE("Kilometers", "Miles", new KilometerMilesConverter()),
	LITER_GALLON("Liters", "Gallons", new LiterGallonConverter()),
	CELCIUS_fAHRENHEIT("Celcius", "Fahrenheit", new CelciusFahrenheitConverter()),
	;
	private final String metricDesc;
	private final String englishDesc;
	private final Converter converter;
	
	
	private Conversion(String metricDesc, String englishDesc, Converter converter) {
		this.metricDesc = metricDesc;
		this.englishDesc = englishDesc;
		this.converter = converter;
	}
	
	public String getMetricLabel() {
		return metricDesc;
	}
	
	public String getEnglishLabel() {
		return englishDesc;
	}
	
	public String getMetricEnglishLabel() {
		return metricDesc + " to " + englishDesc;
	}
	
	public String getEnglishToMetricLabel() {
		return englishDesc + " to " + metricDesc;
	}
	
	public Converter getConverter() {
		return converter;
	}
	
	public String getDescription() {
		return metricDesc + " - " + englishDesc;
	}
	
	public int getId() {
		return ordinal() + 1;
	}
	
	public static Conversion findConversionByDescription(String description) {
		Conversion found = null;
		for (Conversion conversion : values()) {
			if (conversion.getDescription().equalsIgnoreCase(description)){
				found = conversion;
				break;
			}
		}
		return found;
	}
	
	public static Conversion findConversionByOrdinal(int ordinal) {
		Conversion found = null;
		for (Conversion conversion : values()) {
			if (conversion.ordinal() == ordinal){
				found = conversion;
				break;
			}
		}
		return found;
		
	}
}
