package system.gc.androidapps101_assgnment2.converter;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemSelectedListener, OnClickListener, OnCheckedChangeListener {

	private Spinner conversionSelectionSpinner;
	private Button button;
	private RadioGroup converstionTypeButtonGroup;
	private int conversionSelectedPosition;
	private EditText convertFromEditText; 
	private TextView convertToResultsTextView;
	private TextView convertFromLabel;
	private TextView convertToLabel;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		fillSpinner();
		
		button = (Button)findViewById(R.id.convertButton);
		button.setOnClickListener(this);
		
		converstionTypeButtonGroup = (RadioGroup)findViewById(R.id.converstionTypeRadioGroup);
		converstionTypeButtonGroup.setOnCheckedChangeListener(this);
		
		convertFromEditText = (EditText)findViewById(R.id.convertFromEditText);
		
		convertToResultsTextView = (TextView)findViewById(R.id.convertToResultsTextView);
		
		convertFromLabel = (TextView)findViewById(R.id.convertFromTextView);
		convertToLabel = (TextView)findViewById(R.id.convertToLabelTextView);

		Conversion conversion = Conversion.findConversionByOrdinal(0);
		setLabels(conversion.getMetricLabel(), conversion.getEnglishLabel());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	public void fillSpinner() {
		conversionSelectionSpinner = (Spinner)findViewById(R.id.conversionSpinner);
		conversionSelectionSpinner.setOnItemSelectedListener(this);
		List<String> spinnerList = new ArrayList<String>();
		Conversion[] conversions = Conversion.values();
		for (Conversion conversion : conversions) {
			spinnerList.add(conversion.getDescription());
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, spinnerList);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		conversionSelectionSpinner.setAdapter(dataAdapter);
		
	}

	protected void setLabels(String convertFrom, String convertTo) {
		String convertFromBaseLabel = getResources().getString(R.string.convertFrom);
		convertFromLabel.setText(convertFromBaseLabel + " " + convertFrom);
		
		String convertToBaseLabel = getResources().getString(R.string.convertTo);
		convertToLabel.setText(convertToBaseLabel + " " + convertTo);
		
	}
	

	@Override
	public void onClick(View v) {

		convertAndDisplayResults();
	}

	
	private void convertAndDisplayResults() {
		int clickedRadioId = converstionTypeButtonGroup.getCheckedRadioButtonId();

		Conversion conversion = Conversion.findConversionByOrdinal(conversionSelectedPosition);

		Converter converter = conversion.getConverter();
		
		
		Editable strValueToConvert = convertFromEditText.getText();
		
		if (strValueToConvert != null && !strValueToConvert.toString().trim().equals("")) {
			double valueToConvert = 0;
			try {
				valueToConvert = Double.valueOf(strValueToConvert.toString().trim());
			} catch (NumberFormatException e) {
			    Toast.makeText(this, getResources().getString(R.string.convertFromNANErrorMsg), Toast.LENGTH_LONG).show();
			}
	
			double conversionResult = 0;
			switch (clickedRadioId) {
				case R.id.metricToEnglishRadio:
					conversionResult = converter.convertMetricToEnglish(valueToConvert);
					
					break;
		
				case R.id.englishToMetricRadio:
					conversionResult = converter.convertEnglishToMetric(valueToConvert);
					
					break;
				default:
					break;
			}
			
			convertToResultsTextView.setText(StringFormatterUtil.formatDouble(conversionResult));
		} else {
		    Toast.makeText(this, getResources().getString(R.string.emptyConvertFromErrorMsg), Toast.LENGTH_LONG).show();
		}
	}

	
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		conversionSelectedPosition = position;
	
		updateLabels();
		
		convertAndDisplayResults();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		updateLabels();
		convertAndDisplayResults();
		
	}

	private void updateLabels() {
		Conversion conversion = Conversion.findConversionByOrdinal(conversionSelectedPosition);

		int clickedRadioId = converstionTypeButtonGroup.getCheckedRadioButtonId();
		switch (clickedRadioId) {
			case R.id.metricToEnglishRadio:
				setLabels(conversion.getMetricLabel(), conversion.getEnglishLabel());
				
				break;
	
			case R.id.englishToMetricRadio:
				setLabels(conversion.getEnglishLabel(), conversion.getMetricLabel());
				
				break;
			default:
				break;
		}
	}
}
