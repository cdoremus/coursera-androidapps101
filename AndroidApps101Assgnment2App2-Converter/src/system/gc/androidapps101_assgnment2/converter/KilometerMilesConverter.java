package system.gc.androidapps101_assgnment2.converter;

public class KilometerMilesConverter extends Converter{

	public KilometerMilesConverter() {
	}

	@Override
	public double convertMetricToEnglish(double valueToConvert) {
		double milesPerKm = 1.0d/1.60934;
		return valueToConvert * milesPerKm;
	}

	@Override
	public double convertEnglishToMetric(double valueToConvert) {
		double kmPerMiles = 1.60934;
		return valueToConvert * kmPerMiles;
	}

}
