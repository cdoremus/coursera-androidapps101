package system.gc.androidapps101_assgnment2.converter;

public enum ConversionType {
	METRIC_TO_ENGLISH(R.id.metricToEnglishRadio),
	ENGLISH_TO_METRIC(R.id.englishToMetricRadio)
	;
	
	
	private final int radioId;
	
	private ConversionType(int radioId) {
		this.radioId = radioId;
	}
}
