package system.gc.androidapps101_assgnment2.converter;

public abstract class Converter implements MetricToEnglishConverter,
		EnglishToMetricConverter {

	@Override
	public abstract double convertEnglishToMetric(double valueToConvert);

	@Override
	public abstract double convertMetricToEnglish(double valueToConvert);
	

}
