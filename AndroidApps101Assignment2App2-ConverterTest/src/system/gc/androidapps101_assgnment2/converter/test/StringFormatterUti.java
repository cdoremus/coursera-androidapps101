package system.gc.androidapps101_assgnment2.converter.test;

import system.gc.androidapps101_assgnment2.converter.StringFormatterUtil;
import junit.framework.TestCase;

public class StringFormatterUti extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testFormatDouble_Zero() {
		
		assertEquals("0.00", StringFormatterUtil.formatDouble(0.0d));
	}


	public void testFormatDouble_OneThousandWithCommas() {
		
		assertEquals("1,000.00", StringFormatterUtil.formatDouble(1000.0));
	}


	public void testFormatDouble_NinetyNinePoint126_RoundsCorrectly() {
		
		assertEquals("99.13", StringFormatterUtil.formatDouble(99.126));
	}


	public void testFormatDouble_NinetyNinePoint125_RoundsDownCorrectly() {
		
		assertEquals("99.12", StringFormatterUtil.formatDouble(99.125));
	}


	public void testFormatDouble_NinetyNinePoint2_AddsZeroSecondDecimalPlace() {
		
		assertEquals("99.20", StringFormatterUtil.formatDouble(99.2));
	}
}
