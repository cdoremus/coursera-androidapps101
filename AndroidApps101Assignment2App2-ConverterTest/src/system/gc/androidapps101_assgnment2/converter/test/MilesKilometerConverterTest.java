package system.gc.androidapps101_assgnment2.converter.test;

import system.gc.androidapps101_assgnment2.converter.KilometerMilesConverter;
import junit.framework.TestCase;

public class MilesKilometerConverterTest extends TestCase {

	private KilometerMilesConverter converter;
	
	protected void setUp() throws Exception {
		super.setUp();
		converter = new KilometerMilesConverter();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testMilesKilometerConverter() {
		assertNotNull(converter);
	}

	public void testConvertMetricToEnglish() {
		assertEquals(12.0d, converter.convertMetricToEnglish(12.0d));
	}

	public void testConvertEnglishToMetric() {
		assertEquals(19.31d, converter.convertEnglishToMetric(12.0d));
	}

}
